#include <iostream>

#include "aggregate/aggregate.h"

int main(int agrc, char* argv[]) {
  confluo::aggregate_list agg(confluo::primitive_types::INT_TYPE(), confluo::aggregate_manager::get_aggregator("sum"));

  return 0;
}
